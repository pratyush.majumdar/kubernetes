# Kubernetes
Kubernetes, also known as K8s, is an open-source system for automating deployment, scaling, and management of containerized applications. Kubernetes defines a set of building blocks ("primitives") that collectively provide mechanisms that deploy, maintain, and scale applications based on CPU, memory or custom metrics. Kubernetes is loosely coupled and extensible to meet the needs of different workloads. The internal components as well as extensions and containers that run on Kubernetes rely on the Kubernetes API. The platform exerts its control over compute and storage resources by defining resources as objects, which can then be managed as such.

## Kubernetes Cluster Architecture

![Kubernetes Cluster Architecture](https://kubernetes.io/images/docs/kubernetes-cluster-architecture.svg)

## Minikube Cluster Architecture

![Minikube Cluster Architecture](https://kubernetes.io/docs/tutorials/kubernetes-basics/public/images/module_01_cluster.svg)


## 1. Control plane
1. etcd
2. API server
3. Scheduler
4. Controllers

## 2. Nodes
1. kubelet
2. kube-proxy
3. Pods

## Install Minikube
```
# download location
https://minikube.sigs.k8s.io/docs/start/

# for Mac users
brew install minikube
```

## Start Minikube Cluster
```
minikube start --driver docker

😄  minikube v1.16.0 on Darwin 10.14.3
✨  Using the docker driver based on existing profile
👍  Starting control plane node minikube in cluster minikube
🤷  docker "minikube" container is missing, will recreate.
🔥  Creating docker container (CPUs=2, Memory=1999MB) ...
🐳  Preparing Kubernetes v1.20.0 on Docker 20.10.0 ...
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## Minikube Status
```
minikube status

minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
timeToStop: Nonexistent
```

## Minikube Dashboard
```
minikube dashboard

🔌  Enabling dashboard ...
🤔  Verifying dashboard health ...
🚀  Launching proxy ...
🤔  Verifying proxy health ...
🎉  Opening http://127.0.0.1:52529/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
```

## Minikube Stop
```
minikube stop

✋  Stopping node "minikube"  ...
🛑  Powering off "minikube" via SSH ...
🛑  1 nodes stopped.
```

## Basic k8s commands
```
1. kubectl apply -f nginx-deployment.yml
deployment.apps/nginx-deployment created
service/nginx-service created

2. kubectl get node

NAME       STATUS   ROLES                  AGE     VERSION
minikube   Ready    control-plane,master   3m15s   v1.20.0

3. kubectl get pod

NAME                               READY   STATUS    RESTARTS   AGE
nginx-deployment-585449566-5m99t   1/1     Running   0          28s

4. kubectl get service

NAME            TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
kubernetes      ClusterIP   10.96.0.1       <none>        443/TCP        59d
nginx-service   NodePort    10.97.225.237   <none>        80:30100/TCP   46s

5. kubectl get all

NAME                                   READY   STATUS    RESTARTS   AGE
pod/nginx-deployment-585449566-5m99t   1/1     Running   0          60s

NAME                    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
service/kubernetes      ClusterIP   10.96.0.1       <none>        443/TCP        59d
service/nginx-service   NodePort    10.97.225.237   <none>        80:30100/TCP   60s

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-deployment   1/1     1            1           60s

NAME                                         DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-deployment-585449566   1         1         1       60s
```

## Get Service URL to access the service from browser (for docker cluster only)
```
minikube service -- nginx-service

|-----------|---------------|-------------|---------------------------|
| NAMESPACE |     NAME      | TARGET PORT |            URL            |
|-----------|---------------|-------------|---------------------------|
| default   | nginx-service |          80 | http://192.168.49.2:30100 |
|-----------|---------------|-------------|---------------------------|
🏃  Starting tunnel for service nginx-service.
|-----------|---------------|-------------|------------------------|
| NAMESPACE |     NAME      | TARGET PORT |          URL           |
|-----------|---------------|-------------|------------------------|
| default   | nginx-service |             | http://127.0.0.1:50042 |
|-----------|---------------|-------------|------------------------|
🎉  Opening service default/nginx-service in default browser...
❗  Because you are using a Docker driver on darwin, the terminal needs to be open to run it


^C✋  Stopping tunnel for service nginx-service.
```

## Delete the deployments and the services
```
kubectl delete deployment nginx-deployment

deployment.apps "nginx-deployment" deleted

kubectl delete service nginx-service

service "nginx-service" deleted
```

## Get Minikube IP
```
minikube ip

192.168.49.2
```